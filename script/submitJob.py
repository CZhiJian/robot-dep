#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   submitJob.py    
@Contact :   512759438@qq.com
@Author  :   Jian
@datetime:2023/2/14 10:38
'''

'''提交任务到远程集群'''
'''看看公司的机器能不能连到同一个集群，不能的话每台笔记本当成一个主机再用遍历他们的IP提交任务'''
from ray.job_submission import JobSubmissionClient, JobStatus
import time

# If using a remote cluster, replace 127.0.0.1 with the head node's IP address.
client = JobSubmissionClient("http://101.33.255.182:8265")
# client = JobSubmissionClient("http://127.0.0.1:8265")
job_id = client.submit_job(
    # Entrypoint shell command to execute
    entrypoint="python main.py",
    # Path to the local directory that contains the script.py file
    # runtime_env={"working_dir": "./",
    runtime_env={"working_dir": "/Users/mingjiexiao/PycharmProjects/FunJoyTest/dnf",
        #依赖 "pip": ["requests==2.26.0"]
                 }
)
print(job_id)

def wait_until_status(job_id, status_to_wait_for, timeout_seconds=10):
    start = time.time()
    while time.time() - start <= timeout_seconds:
        status = client.get_job_status(job_id)
        print(f"status: {status}")
        if status in status_to_wait_for:
            break
        time.sleep(1)


wait_until_status(job_id, {JobStatus.SUCCEEDED, JobStatus.STOPPED, JobStatus.FAILED})
logs = client.get_job_logs(job_id)
print(logs)
# 强制停止
client.stop_job(job_id)