#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   test.py    
@Contact :   512759438@qq.com
@Author  :   Jian
@datetime:2023/2/11 00:48
'''


# script.py
# import ray
#
# @ray.remote
# def hello_world():
#     return "hello world"
#
# # Automatically connect to the running Ray cluster.
# ray.init()
# print(ray.get(hello_world.remote()))



from collections import Counter
import socket
import time
import ray
# ray.init(address='auto', _node_ip_address='101.33.255.182')
ray.init(address='auto')
# ray.init(address='auto', num_cpus=1)
# ray.init(address='ray://101.33.255.182:10001')
print('''This cluster consists o    f
    {} nodes in total
    {} CPU resources in total
'''.format(len(ray.nodes()), ray.cluster_resources()['CPU']))
@ray.remote(num_cpus=1)
def f():
    time.sleep(0.001)
    # Return IP address.
    return socket.gethostbyname(socket.gethostname())
object_ids = [f.remote() for _ in range(10000)]
ip_addresses = ray.get(object_ids)
print('Tasks executed')
for ip_address, num_tasks in Counter(ip_addresses).items():
    print('    {} tasks on {}'.format(num_tasks, ip_address))



# ray job submit --no-wait --working-dir your_working_directory -- python script.py
# ray job submit --working-dir /Users/mingjiexiao/PycharmProjects/robotDep -- python test.py
# ray job submit --address='101.33.255.182' --working-dir . -- python test.py